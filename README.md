# README #

This is a simple demo database that will show you various ways to expose data using JSON. The demo database is supported by the presentations from [DanNotes 19-20 November 2014 in Korsør](http://www.slideshare.net/JohnDalsgaard/dannotes-19-20-november-2014), [EngageUG 30-31 March 2015 in Ghent](http://www.slideshare.net/JohnDalsgaard/engage-ug-rest-services-2015), and ICONUK 21-22 September 2015 in London.

In addition this database is built on top of my previous [MVC Demo database](https://bitbucket.org/john_dalsgaard/demo-apps). Just take that as an extra benefit ;-)

### Purpose of this app. ###

The purpose is to show many ways of getting data as JSON via RESTful services:
* Domino Access Services
* Domino calendar services
* Domino data services
* Extension Library
* Build your own (built-in JSON, GSON, and .... LotusScript!)

### How do I get set up? ###
Please note that the Java examples in the demo database require you to use the [OpenNTF Domino API](http://www.openntf.org/main.nsf/project.xsp?r=project/OpenNTF%20Domino%20API). The version dated early 2015 (RC3 or later) is Ok for this project to run.

NB! Please note that you probably need to switch to the XPages perspective (under Window / Open Perspective / Other... in Domino Designer) to make the "Team Development" actions available.

* Download the project (it is an on-disk-project of the NSF)
* Create a new NSF from the repository (the on disk project is in the Nsf subdirectory):
 * - Go to Package Explorer and create a New Project
 * - Give it a name, e.g. "JSON Demo ODP" and select the "NSF" subdirectory of the ODP project as path
 * - Choose file system: Notes Virtual File System
 * - Right-click the ODP project and select "Team Development" and "Associate with new NSF..."
 * - Select a server and database name and press [Ok]
* ... and now you are ready to explore and play with the code. You will need to add some test data yourself.

If you want to try the calendar service - do the following:

* Create a new mail database named "cal.nsf" in the same directory as where you put this database
* It must be based on the "Mail 9" template (or later, I guess...)
* Add some appointments, meetings, etc. in it.

If you want to use the GSON example in this database then you will have to make GSON available to your project. I have done that through an update site (as an OSGi plug-in) - .... and I have also added the plugin, feature, and update projects to this repo :-) - you still need to deploy it as outlined in the article below.

You can, however, also use the quick and dirty way by putting the GSON jar in the jvm/lib/ext library of your server:

* Download [GSON](https://code.google.com/p/google-gson/downloads/detail?name=google-gson-2.2.4-release.zip&)

You can also take it one step further and wrap the jar file into an OSGi plug-in as I have done :-) Then you can use your own package names. See these articles for steps to follow:

* [Wrap an existing JAR file into a plug-in](http://www.dalsgaard-data.eu/blog/wrap-an-existing-jar-file-into-a-plug-in/)
* [Deploy an Eclipse update site to IBM Domino and IBM Domino Designer](http://www.dalsgaard-data.eu/blog/deploy-an-eclipse-update-site-to-ibm-domino-and-ibm-domino-designer/)

### Test in Google Postman ###
I have added the entire set of tests used in the presentations as a collection that you can import into Postman (file name: "REST.json.postman_collection"). Please note that you will need to:

* Add you own basic authentication - I have replaced mine with "...your_own..."
* Replace the server name and path: "json.dalsgaard-data.dk/demo/" with your own server and path to the database

### Who do I talk to? ###

* Any questions? You may contact me on: john@dalsgaard-data.dk
* You can also see more about what I and we do - and follow my blog on our [homepage](http://www.dalsgaard-data.eu/)