package dk.dalsgaarddata.demo.dao.facade;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import dk.dalsgaarddata.demo.base.BaseCRUDFacade;
import dk.dalsgaarddata.demo.dao.CarDAO;
import dk.dalsgaarddata.demo.dao.DominoCarDAO;
import dk.dalsgaarddata.demo.data.Car;

/* 2013.11.13/Jda
 * Service layer class implementing business rules for working with Car data
 */
public class CarCRUDFacade extends BaseCRUDFacade {

	private static final long serialVersionUID = 1L;
	private Map<String, Car> cars = null;

	// An instance of a DAO to work with data
	private CarDAO getDao() {
		CarDAO dao = new DominoCarDAO();
		dao.setRunAsSigner(runAsSigner);
		return dao;
	}

	private boolean removeFromCache(String existingKey) {
		if (null != this.cars) {
			if (null != existingKey) {
				debug("updateInMemory: remove... " + existingKey);
				this.cars.remove(existingKey);
				return true;
			}
		}
		return false;
	}

	private void updateCache(Car car) {
		if (null != this.cars) {
			if (null != car) {
				if (this.cars.containsKey(car.getKey())) {
					debug("updateInMemory: update... " + car.getKey());
				} else {
					debug("updateInMemory: add... " + car.getKey());
				}
				this.cars.put(car.getKey(), car);
			}
		}
	}

	private List<Car> sortByBrand(Map<String, Car> map) {
		// Sort values based on name
		if (null == map) {
			return new ArrayList<Car>();
		}
		List<Car> list = new ArrayList<Car>(map.values());
		Collections.sort(list, new Comparator<Car>() {
			public int compare(Car c1, Car c2) {
				return c1.getBrand().compareToIgnoreCase(c2.getBrand());
			}
		});
		return list;
	}

	private Map<String, Car> getCachedCars() {
		if (null == cars) {
			debug("Reading all cars into cache...");
			cars = getDao().getAllCars();
		}
		return cars;
	}

	public List<Car> getAllCars() {
		return sortByBrand(getCachedCars());
	}

	public Car findCar(String key) {
		return getCachedCars().get(key);
	}

	public void saveCar(Car car) {
		getDao().saveCar(car);
		updateCache(car);
	}

	public void removeCar(Car car) {
		removeFromCache(car.getKey());
		getDao().removeCar(car);
	}
}
