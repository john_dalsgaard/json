package dk.dalsgaarddata.demo.dao.facade;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.faces.validator.ValidatorException;
import javax.xml.bind.ValidationException;

import com.ibm.commons.util.StringUtil;

import dk.dalsgaarddata.demo.base.BaseCRUDFacade;
import dk.dalsgaarddata.demo.base.Util;
import dk.dalsgaarddata.demo.bean.DataBean;
import dk.dalsgaarddata.demo.dao.DominoPersonDAO;
import dk.dalsgaarddata.demo.dao.PersonDAO;
import dk.dalsgaarddata.demo.data.Car;
import dk.dalsgaarddata.demo.data.Person;

/* 2013.11.13/Jda
 * Service layer class implementing business rules for working with Person data
 */
public class PersonCRUDFacade extends BaseCRUDFacade {

	private static final long serialVersionUID = 1L;
	public static final String MUST_FILL_NAME = "You must enter a name";
	public static final String MUST_FILL_BRAND = "Select a brand for all cars";
	public static final String MUST_FILL_MODEL = "Enter a model for each car";
	public static final String MUST_FILL_COLOUR = "Enter a colour for each car";
	private Map<String, Person> persons = null;

	// An instance of a DAO to work with data
	private PersonDAO getDao() {
		PersonDAO dao = new DominoPersonDAO();
		debug("runAsSigner=" + runAsSigner);
		dao.setRunAsSigner(runAsSigner);
		return dao;
	}

	private boolean removeFromCache(String existingKey) {
		if (null != this.persons) {
			if (null != existingKey) {
				debug("updateInMemory: remove... " + existingKey);
				this.persons.remove(existingKey);
				return true;
			}
		}
		return false;
	}

	private void updateCache(Person person) {
		if (null != this.persons) {
			if (null != person) {
				if (this.persons.containsKey(person.getKey())) {
					debug("updateInMemory: update... " + person.getKey());
				} else {
					debug("updateInMemory: add... " + person.getKey());
				}
				this.persons.put(person.getKey(), person);
			}
		}
	}

	private List<Person> sortByName(Map<String, Person> map) {
		// Sort values based on name
		if (null == map) {
			return new ArrayList<Person>();
		}
		List<Person> list = new ArrayList<Person>(map.values());
		Collections.sort(list, new Comparator<Person>() {
			public int compare(Person c1, Person c2) {
				return c1.getName().compareToIgnoreCase(c2.getName());
			}
		});
		return list;
	}

	private Map<String, Person> getCachedPersons() {
		if (null == persons) {
			debug("Reading all persons into cache... ");
			persons = getDao().getAllPersons();
		}
		return persons;
	}

	public List<Person> getAllPersons() {
		return sortByName(getCachedPersons());
	}

	public Person findPerson(String key) {
		return getCachedPersons().get(key);
	}

	public List<Car> getPersonsCars(String personKey) {
		List<Car> list = new ArrayList<Car>();
		if (StringUtil.isNotEmpty(personKey)) {
			for (Car car : DataBean.getCurrentInstance().getCarFacade().getAllCars()) {
				if (personKey.equalsIgnoreCase(car.getPersonKey())) {
					list.add(car);
				}
			}
		}
		return list;
	}

	/* 
	 * All validation routines. Can be called from a validator or the generic 
	 * validate methods in this class.
	 */
	public void validateName(String name) throws ValidatorException {
		if (StringUtil.isEmpty(name)) {
			throwValidationError(MUST_FILL_NAME);
		} else if (name.length() < 3) {
			throwValidationError("Name must be at least 3 characters long");
		}
	}

	public void validateAddress(String address, String zip, String city) throws ValidatorException {
		if (StringUtil.isNotEmpty(address)) {
			if (StringUtil.isEmpty(zip) || StringUtil.isEmpty(city)) {
				throwValidationError("If you enter an address you must also enter a zip code and a city");
			}
		}
	}

	public void validateZip(String zip, String address, String city) throws ValidatorException {
		if (StringUtil.isNotEmpty(zip)) {
			if (StringUtil.isEmpty(address) || StringUtil.isEmpty(city)) {
				throwValidationError("If you enter a zip code you must also enter an address and a city");
			}
		}
	}

	public void validateCity(String city, String address, String zip) throws ValidatorException {
		if (StringUtil.isNotEmpty(city)) {
			if (StringUtil.isEmpty(address) || StringUtil.isEmpty(zip)) {
				throwValidationError("If you enter a city you must also enter an address and a zip code");
			}
		}
	}

	public void validateEmail(String email) throws ValidatorException {
		if (StringUtil.isNotEmpty(email)) {
			if (!Util.isEmail(email)) {
				throwValidationError("'" + email + "' is not a valid email address");
			}
		}
	}

	public void validateColour(String colour) throws ValidatorException {
		if (StringUtil.isNotEmpty(colour)) {
			if (colour.length() < 3) {
				throwValidationError("Colour must be at least 3 chars");
			}
		}
	}

	public void validatePerson(Person person) throws ValidationException {
		/*
		 * Register a new person or update an existing. Check:
		 * 1. Name is filled in
		 * 2. Address, zip and city are all filled in - or none of them
		 * 3. Email is a valid email address - if filled in
		 * 
		 * This method uses the same validator methods called directly by the validator.
		 */
		if (null == person) {
			error("Ooops - person is null???");
			// ... and some meaningfull escape...
		}
		debug("Validate person");
		initErrors();
		try {
			validateName(person.getName());
		} catch (ValidatorException ve) {
			addErrorMessage("Name", ve.getMessage());
		}
		try {
			validateAddress(person.getAddress(), person.getZip(), person.getCity());
		} catch (ValidatorException ve) {
			addErrorMessage("Address", ve.getMessage());
		}
		try {
			validateZip(person.getZip(), person.getAddress(), person.getCity());
		} catch (ValidatorException ve) {
			addErrorMessage("Zip", ve.getMessage());
		}
		try {
			validateCity(person.getCity(), person.getAddress(), person.getZip());
		} catch (ValidatorException ve) {
			addErrorMessage("City", ve.getMessage());
		}
		try {
			validateEmail(person.getEmail());
		} catch (ValidatorException ve) {
			addErrorMessage("Email", ve.getMessage());
		}
		// TODO - traverse all cars and call relevant validate methods for each row

		if (isError()) {
			throwValidationErrors();
		}
	}

	public void savePerson(Person person) throws ValidationException {
		validatePerson(person);
		getDao().savePerson(person);
		updateCache(person);
	}

	public void removePerson(Person person) {
		removeFromCache(person.getKey());
		getDao().removePerson(person);
	}
}
