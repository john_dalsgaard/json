/**
 * Concrete implementation of a CarDAO with the use of a Domino database
 */
package dk.dalsgaarddata.demo.dao;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.openntf.domino.Document;
import org.openntf.domino.View;
import org.openntf.domino.ViewEntry;

import com.ibm.commons.util.StringUtil;

import dk.dalsgaarddata.demo.base.BaseDominoDAO;
import dk.dalsgaarddata.demo.data.Car;

/**
 * @author jda, 2013.11.13
 * 
 */
public class DominoCarDAO extends BaseDominoDAO implements CarDAO {

	private static final String VIEW_NAME = "Cars";
	private static final String FORM_NAME = "Car";

	private View getView() {
		return super.getView(VIEW_NAME);
	}

	private Car loadFromEntry(ViewEntry entry) {
		// Create a new object and load it with data
		Car car = new Car();
		car.setKey((String) entry.getColumnValues().get(0));
		car.setPersonKey((String) entry.getColumnValues().get(1));
		car.setBrand((String) entry.getColumnValues().get(2));
		car.setModel((String) entry.getColumnValues().get(3));
		car.setColour((String) entry.getColumnValues().get(4));
		car.setConvertible(toBoolean((String) entry.getColumnValues().get(5)));
		car.setDoors(toInteger(entry.getColumnValues().get(6)));
		car.setUnid(entry.getUniversalID());
		return car;
	}

	private Car loadFromDocument(Document doc) {
		// Create a new object and load it with data
		Car car = new Car();
		car.setKey(doc.getItemValueString("Key"));
		car.setPersonKey(doc.getItemValueString("PersonKey"));
		car.setBrand(doc.getItemValueString("Brand"));
		car.setModel(doc.getItemValueString("Model"));
		car.setColour(doc.getItemValueString("Colour"));
		car.setConvertible(toBoolean(doc.getItemValueString("Convertible")));
		car.setDoors(toInteger(doc.getItemValueString("Doors")));
		car.setUnid(doc.getUniversalID());
		return car;
	}

	public Map<String, Car> getAllCars() {
		if (isViewDirty()) {
			getView().refresh();
		}
		Map<String, Car> all = new ConcurrentHashMap<String, Car>();
		for (ViewEntry entry : getView().getAllEntries()) {
			Car car = loadFromEntry(entry);
			all.put(car.getKey(), car);
		}
		return all;
	}

	public Car loadCar(String key) {
		Car car = null;
		if (!StringUtil.isEmpty(key)) {
			if (isViewDirty()) {
				getView().refresh();
			}
			if (isUnid(key)) {
				Document doc = getDb().getDocumentByUNID(key);
				if (null != doc) {
					car = loadFromDocument(doc);
				}
			} else {
				ViewEntry entry = getView().getEntryByKey(key);
				if (null != entry) {
					car = loadFromEntry(entry);
				}
			}
		}
		return car;
	}

	public void saveCar(Car car) {
		Document doc = null;
		if (!StringUtil.isEmpty(car.getKey())) {
			doc = getView().getDocumentByKey(car.getKey());
		}
		if (null == doc) {
			doc = getDb().createDocument();
			car.setUnid(doc.getUniversalID());
			updateField(doc, "Unid", car.getUnid());
			if (StringUtil.isEmpty(car.getKey())) {
				car.setKey(car.getUnid());
			}
		}
		updateField(doc, "Key", car.getKey());
		updateField(doc, "PersonKey", car.getPersonKey());
		updateField(doc, "Brand", car.getBrand());
		updateField(doc, "Model", car.getModel());
		updateField(doc, "Colour", car.getColour());
		updateField(doc, "Convertible", car.isConvertible());
		updateField(doc, "Doors", car.getDoors());
		updateField(doc, "Form", FORM_NAME);
		if (doc.isDirty()) {
			doc.save();
			setViewDirty(true);
		}
	}

	public void removeCar(Car car) {
		if (null == car) return;
		if (!StringUtil.isEmpty(car.getKey())) {
			Document doc = getView().getDocumentByKey(car.getKey());
			if (null != doc) {
				doc.remove(true);
				car = null;
				setViewDirty(true);
			}
		}
	}

	public void removeCar(String key) {
		removeCar(loadCar(key));
	}

}
