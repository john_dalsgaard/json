package dk.dalsgaarddata.demo.dao;

import java.util.Map;

import dk.dalsgaarddata.demo.data.Car;

/**
 * @author jda, 2013.11.13
 */
public interface CarDAO {

	/*
	 * Read a car document from the database and return it as a Car object
	 * @param key	A unique key for the Car document
	 * @return 		A Car object loaded with the record data - or null if not found
	 */
	public Car loadCar(String key);

	/*
	 * Save car information to the database. This will overwrite existing data if the car already exists
	 * or create a new record if the car is not currently in the database
	 * @param car	A Car object loaded with data
	 */
	public void saveCar(Car car);

	/*
	 * Retrieve a list of all cars in the system
	 * @return	A list of Car objects with data loaded for all cars in the system or an empty list if no car found
	 */
	public Map<String, Car> getAllCars();

	/*
	 * Remove the car from the system.
	 * @param car	A Car object loaded with data
	 */
	public void removeCar(Car car);

	/*
	 * Remove a car with the key from the system.
	 * @param key	A key to find a Car object
	 */
	public void removeCar(String key);

	/*
	 * Set the DAO to run as signer
	 */
	public void setRunAsSigner(boolean runAsSigner);

}
