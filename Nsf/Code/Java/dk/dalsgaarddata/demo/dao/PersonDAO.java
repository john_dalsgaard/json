package dk.dalsgaarddata.demo.dao;

import java.util.Map;

import dk.dalsgaarddata.demo.data.Person;

/**
 * @author jda, 2013.11.13
 */
public interface PersonDAO {

	/*
	 * Read a person document from the database and return it as a Person object
	 * @param key	A unique key for the Person document
	 * @return 		A Person object loaded with the record data - or null if not found
	 */
	public Person loadPerson(String key);

	/*
	 * Save person information to the database. This will overwrite existing data if the person already exists
	 * or create a new record if the person is not currently in the database
	 * @param person	A Person object loaded with data
	 */
	public void savePerson(Person person);

	/*
	 * Retrieve a list of all persons in the system
	 * @return	A list of Person objects with data loaded for all persons in the system or an empty list if no person found
	 */
	public Map<String, Person> getAllPersons();

	/*
	 * Remove the person from the system.
	 * @param person	A Person object loaded with data
	 */
	public void removePerson(Person person);

	/*
	 * Remove a person with the key from the system.
	 * @param key	A key to find a Person object
	 */
	public void removePerson(String key);

	/*
	 * Set the DAO to run as signer
	 */
	public void setRunAsSigner(boolean runAsSigner);

}
