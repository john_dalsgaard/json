package dk.dalsgaarddata.demo.bean;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;

import javax.faces.context.ResponseWriter;
import javax.servlet.http.HttpServletResponse;

import com.ibm.commons.util.StringUtil;
import com.ibm.domino.services.util.JsonWriter;

import dk.dalsgaarddata.demo.base.BaseBean;
import dk.dalsgaarddata.demo.base.Util;
import dk.dalsgaarddata.demo.data.Car;
import dk.dalsgaarddata.demo.view.PersonView;

public class JSONBean extends BaseBean implements Serializable {

	private static final long serialVersionUID = 1L;

	public void getAllPersons() {
		try {
			debug("create JSON for all persons and their cars");
			HttpServletResponse response = (HttpServletResponse) Util.getExternalContext().getResponse();
			ResponseWriter out = Util.getFacesContext().getResponseWriter();
			response.setContentType("application/json");
			response.setHeader("Cache-Control", "no-cache");
			response.setCharacterEncoding("utf-8");
			List<PersonView> persons = DataBean.getCurrentInstance().getPersons();
			StringWriter sw = new StringWriter();
			JsonWriter jw = new JsonWriter(sw, false);
			jw.startArray();
			for (PersonView person : persons) {
				jw.startArrayItem();
				jw.startObject();
				writePersonInfo(jw, person);
				jw.startProperty("cars");
				jw.startArray();
				for (Car car : person.getCars()) {
					writeCarInfo(jw, car);
				}
				jw.endArray();
				jw.endProperty();
				jw.endObject();
				jw.endArrayItem();

			}
			jw.endArray();
			jw.flush();
			jw.close();
			out.write(sw.toString());
			response.setStatus(200);
		} catch (Exception e) {
			error("getAllPersons(): " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		// Stop the page from further processing;
		Util.getFacesContext().responseComplete();

	}

	private void writeCarInfo(JsonWriter jw, Car car) throws IOException {
		jw.startArrayItem();
		jw.startObject();
		jw.startProperty("key");
		jw.outStringLiteral(car.getKey());
		jw.endProperty();
		jw.startProperty("brand");
		jw.outStringLiteral(car.getBrand());
		jw.endProperty();
		if (StringUtil.isNotEmpty(car.getModel())) {
			jw.startProperty("model");
			jw.outStringLiteral(car.getModel());
			jw.endProperty();
		}
		if (StringUtil.isNotEmpty(car.getColour())) {
			jw.startProperty("colour");
			jw.outStringLiteral(car.getColour());
			jw.endProperty();
		}
		jw.startProperty("convertible");
		jw.outBooleanLiteral(car.isConvertible());
		jw.endProperty();
		if (null != car.getDoors()) {
			jw.startProperty("yearBorn");
			jw.outIntLiteral(car.getDoors());
			jw.endProperty();
		}
		jw.endObject();
		jw.endArrayItem();
	}

	private void writePersonInfo(JsonWriter jw, PersonView person) throws IOException {
		jw.startProperty("key");
		jw.outStringLiteral(person.getPerson().getKey());
		jw.endProperty();
		jw.startProperty("name");
		jw.outStringLiteral(person.getPerson().getName());
		jw.endProperty();
		if (StringUtil.isNotEmpty(person.getPerson().getAddress())) {
			jw.startProperty("address");
			jw.outStringLiteral(person.getPerson().getAddress());
			jw.endProperty();
			jw.startProperty("zip");
			jw.outStringLiteral(person.getPerson().getZip());
			jw.endProperty();
			jw.startProperty("city");
			jw.outStringLiteral(person.getPerson().getCity());
			jw.endProperty();
		}
		if (StringUtil.isNotEmpty(person.getPerson().getEmail())) {
			jw.startProperty("email");
			jw.outStringLiteral(person.getPerson().getEmail());
			jw.endProperty();
		}
		if (null != person.getPerson().getYearBorn()) {
			jw.startProperty("yearBorn");
			jw.outIntLiteral(person.getPerson().getYearBorn());
			jw.endProperty();
		}
		jw.startProperty("numberOfCars");
		jw.outIntLiteral(person.getCarCount());
		jw.endProperty();
	}

}
