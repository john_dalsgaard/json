package dk.dalsgaarddata.demo.bean;

import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;

import dk.dalsgaarddata.demo.base.BaseBean;
import dk.dalsgaarddata.demo.base.Util;
import dk.dalsgaarddata.demo.dao.facade.PersonCRUDFacade;

public class PersonValidator extends BaseBean implements Serializable, Validator {
	private static final long serialVersionUID = 1L;
	private transient UIComponent addressComponent = null;
	private transient UIComponent zipComponent = null;
	private transient UIComponent cityComponent = null;
	private transient UIComponent rowBrandComponent = null;
	private transient UIComponent rowModelComponent = null;
	private transient UIComponent rowColourComponent = null;
	private transient UIComponent rowConvertibleComponent = null;
	private transient UIComponent rowDoorsComponent = null;

	public String getNameMustFillMessage() {
		return PersonCRUDFacade.MUST_FILL_NAME;
	}

	public String getBrandMustFillMessage() {
		return PersonCRUDFacade.MUST_FILL_BRAND;
	}

	public String getModelMustFillMessage() {
		return PersonCRUDFacade.MUST_FILL_MODEL;
	}

	public String getColourMustFillMessage() {
		return PersonCRUDFacade.MUST_FILL_COLOUR;
	}

	public boolean isBrandRequired(UIComponent component) {
		if (null != component) {
			PersonValidator validator = getValidator();
			if (!Util.isEmptyValue(Util.getSubmittedValue(validator.getRowModelComponent()))) return true;
			if (!Util.isEmptyValue(Util.getSubmittedValue(validator.getRowColourComponent()))) return true;
			if (Util.isCheckBoxChecked(validator.getRowConvertibleComponent())) return true;
			if (!Util.isEmptyValue(Util.getSubmittedValue(validator.getRowDoorsComponent()))) return true;
		}
		return false;
	}

	public boolean isModelRequired(UIComponent component) {
		PersonValidator validator = getValidator();
		if (!Util.isEmptyValue(Util.getSubmittedValue(validator.getRowBrandComponent()))) {
			return Util.isEmptyValue(Util.getSubmittedValue(component));
		}
		return false;
	}

	public boolean isColourRequired(UIComponent component) {
		PersonValidator validator = getValidator();
		if (!Util.isEmptyValue(Util.getSubmittedValue(validator.getRowBrandComponent()))) {
			return Util.isEmptyValue(Util.getSubmittedValue(component));
		}
		return false;
	}

	public void validate(FacesContext context, UIComponent component, java.lang.Object value) {
		debug("validator called: " + component.getId() + "=" + value);
		// Get the current instance of the view bean and the validator bean
		PersonEditBean bean = PersonEditBean.getCurrentInstance();
		PersonValidator validator = getValidator(context);
		if ("inputName".equalsIgnoreCase(component.getId())) {
			bean.getPersonFacade().validateName((String) value);
		}
		if ("inputAddress".equalsIgnoreCase(component.getId())) {
			String zip = (String) Util.getSubmittedValue(validator.getZipComponent());
			String city = (String) Util.getSubmittedValue(validator.getCityComponent());
			bean.getPersonFacade().validateAddress((String) value, zip, city);
		}
		if ("inputZip".equalsIgnoreCase(component.getId())) {
			String address = (String) Util.getSubmittedValue(validator.getAddressComponent());
			String city = (String) Util.getSubmittedValue(validator.getCityComponent());
			bean.getPersonFacade().validateZip((String) value, address, city);
		}
		if ("inputCity".equalsIgnoreCase(component.getId())) {
			String address = (String) Util.getSubmittedValue(validator.getAddressComponent());
			String zip = (String) Util.getSubmittedValue(validator.getZipComponent());
			bean.getPersonFacade().validateCity((String) value, address, zip);
		}
		if ("inputEmail".equalsIgnoreCase(component.getId())) {
			bean.getPersonFacade().validateEmail((String) value);
		}
		// Example of validating a component of a row...
		if ("rowColour".equalsIgnoreCase(component.getId())) {
			bean.getPersonFacade().validateColour((String) value);
		}

	}

	private PersonValidator getValidator() {
		return getValidator(Util.getFacesContext());
	}

	private PersonValidator getValidator(FacesContext context) {
		return (PersonValidator) context.getApplication().getVariableResolver().resolveVariable(context, "PersonValidator");
	}

	public UIComponent getAddressComponent() {
		return addressComponent;
	}

	public UIComponent getZipComponent() {
		return zipComponent;
	}

	public UIComponent getCityComponent() {
		return cityComponent;
	}

	public void setAddressComponent(UIComponent addressComponent) {
		this.addressComponent = addressComponent;
	}

	public void setZipComponent(UIComponent zipComponent) {
		this.zipComponent = zipComponent;
	}

	public void setCityComponent(UIComponent cityComponent) {
		this.cityComponent = cityComponent;
	}

	public UIComponent getRowBrandComponent() {
		return rowBrandComponent;
	}

	public UIComponent getRowModelComponent() {
		return rowModelComponent;
	}

	public UIComponent getRowColourComponent() {
		return rowColourComponent;
	}

	public UIComponent getRowConvertibleComponent() {
		return rowConvertibleComponent;
	}

	public UIComponent getRowDoorsComponent() {
		return rowDoorsComponent;
	}

	public void setRowBrandComponent(UIComponent rowBrandComponent) {
		this.rowBrandComponent = rowBrandComponent;
	}

	public void setRowModelComponent(UIComponent rowModelComponent) {
		this.rowModelComponent = rowModelComponent;
	}

	public void setRowColourComponent(UIComponent rowColourComponent) {
		this.rowColourComponent = rowColourComponent;
	}

	public void setRowConvertibleComponent(UIComponent rowConvertibleComponent) {
		this.rowConvertibleComponent = rowConvertibleComponent;
	}

	public void setRowDoorsComponent(UIComponent rowDoorsComponent) {
		this.rowDoorsComponent = rowDoorsComponent;
	}

}
