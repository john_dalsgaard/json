package dk.dalsgaarddata.demo.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.xml.bind.ValidationException;

import com.ibm.commons.util.StringUtil;

import dk.dalsgaarddata.demo.base.BaseBean;
import dk.dalsgaarddata.demo.base.Util;
import dk.dalsgaarddata.demo.dao.facade.PersonCRUDFacade;
import dk.dalsgaarddata.demo.data.Car;
import dk.dalsgaarddata.demo.data.Person;

/**
 * @author jda, 2013.11.13
 * 
 */
public class PersonEditBean extends BaseBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Person person = null;
	private List<RowCar> rowCars = null;

	// Important - this property MUST be transient to avoid trying to create a new instance....
	//	private transient PersonCRUDFacade personFacade;

	// Inner helper class to handle entry of cars
	public class RowCar implements Serializable {
		private static final long serialVersionUID = 1L;
		private Car car = null;

		public RowCar() {
			// Need an empty placeholder for new cars entered...
			setCar(new Car());
		}

		public RowCar(Car car) {
			setCar(car);
		}

		public Car getCar() {
			return car;
		}

		public void setCar(Car car) {
			this.car = car;
		}
	}

	public PersonEditBean() {
	}

	public static PersonEditBean getCurrentInstance() {
		// This is a neat way to get a handle on the instance of this bean in the scope from other Java code...
		FacesContext context = Util.getFacesContext();
		PersonEditBean bean = (PersonEditBean) context.getApplication().getVariableResolver().resolveVariable(context, "PersonEdit");
		return bean;
	}

	public void loadPerson(String key) {
		//		setPerson(DataBean.getCurrentInstance().getPersonFacade().findPerson(key));
		setPerson(getPersonFacade().findPerson(key));
		if (null != person) {
			debug("Person loaded: " + person.getName());
		}
	}

	public void savePerson() {
		try {
			debug("save person");
			getPersonFacade().savePerson(getPerson());
			for (RowCar row : getRowCars()) {
				if (StringUtil.isNotEmpty(row.getCar().getBrand())) {
					debug("savePerson - Save car: " + row.getCar().getBrand());
					row.getCar().setPersonKey(getPerson().getKey());
					DataBean.getCurrentInstance().getCarFacade().saveCar(row.getCar());
				}
			}
			debug("person saved");
			Util.redirectToXpage("persons.xsp");
		} catch (ValidationException e) {
			debug("Save error: " + e.getMessage());
			displayErrorMessages(DataBean.getCurrentInstance().getPersonFacade().getErrors());
		}
	}

	public PersonCRUDFacade getPersonFacade() {
		//		if (null == personFacade) {
		//			personFacade = DataBean.getCurrentInstance().getPersonFacade();
		//		}
		//		return personFacade;
		return DataBean.getCurrentInstance().getPersonFacade();
	}

	public void cancelEntry() {
		if (isNeverSaved()) {
			Util.redirectToXpage("persons.xsp");
		} else {
			Util.redirectToXpage("person.xsp?id=" + person.getKey());
		}
	}

	// View methods
	public String getSaveButtonLabel() {
		if (isNeverSaved()) {
			return "Opret";
		} else {
			return "Gem";
		}
	}

	public String getBackLink() {
		if (isNeverSaved()) {
			return "newUser.xsp";
		} else {
			return "editUser.xsp";
		}
	}

	public boolean isNeverSaved() {
		if (null != person) {
			debug("isNeverSaved: Key=" + person.getKey());
			return StringUtil.isEmpty(person.getKey());
		}
		return true;
	}

	// Getters & Setters
	public void setPerson(Person person) {
		this.person = person;
	}

	public Person getPerson() {
		if (null == person) {
			String id = Util.getUrlParam("id");
			if (StringUtil.isEmpty(id)) {
				debug("getPerson. Create new empty Person");
				setPerson(new Person()); // To allow reference to object...
			} else {
				// This is a page opened with a key to an existing fishing trip
				debug("Open existing Person. id=" + id);
				loadPerson(id);
			}
		}
		return person;
	}

	private List<Car> getCars() {
		return DataBean.getCurrentInstance().getPersonFacade().getPersonsCars(getPerson().getKey());
	}

	public List<RowCar> getRowCars() {
		if (null == rowCars) {
			debug("getRowCars: First time...");
			rowCars = new ArrayList<RowCar>();
			for (Car car : getCars()) {
				debug("getRowCars: Add car: " + car);
				rowCars.add(new RowCar(car));
			}
		}
		int empties = 0;
		for (RowCar car : rowCars) {
			if (null == car.getCar() || StringUtil.isEmpty(car.getCar().getBrand())) {
				empties++;
			}
		}
		if (empties == 0) { // No empty rows. Add one
			debug("getRowCars: Add empty row...");
			rowCars.add(new RowCar());
		}
		if (rowCars.size() < 2) { // Initially no values entered - show 2 empty lines
			debug("getRowCars: First time add 2 empty rows...");
			rowCars.add(new RowCar());
		}
		return rowCars;
	}

	public void setRowCars(List<RowCar> rowCars) {
		debug("setRowCars...");
		this.rowCars = rowCars;
	}
}
