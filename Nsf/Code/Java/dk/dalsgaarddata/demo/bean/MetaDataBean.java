package dk.dalsgaarddata.demo.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import dk.dalsgaarddata.demo.base.BaseBean;
import dk.dalsgaarddata.demo.data.Car;

/**
 * @author jda, 2013.11.13
 * 
 */
public class MetaDataBean extends BaseBean implements Serializable {

	private static final long serialVersionUID = 1L;

	public MetaDataBean() {
	}

	public List<SelectItem> getListAcceptableBrands() {
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		// selectItems.add(new SelectItem("", "<v�lg>"));
		selectItems.add(new SelectItem(Car.AcceptedBrands.CORVETTE.getValue()));
		selectItems.add(new SelectItem(Car.AcceptedBrands.PORSCHE.getValue()));
		selectItems.add(new SelectItem(Car.AcceptedBrands.AUDI.getValue()));
		selectItems.add(new SelectItem(Car.AcceptedBrands.FERRARI.getValue()));
		selectItems.add(new SelectItem(Car.AcceptedBrands.MERCEDES.getValue()));
		selectItems.add(new SelectItem(Car.AcceptedBrands.MUSTANG.getValue()));
		selectItems.add(new SelectItem(Car.AcceptedBrands.BMW.getValue()));
		return selectItems;
	}
}