package dk.dalsgaarddata.demo.bean;

import java.io.Serializable;

import dk.dalsgaarddata.demo.base.BaseBean;
import dk.dalsgaarddata.demo.base.BaseCRUDFacade;
import dk.dalsgaarddata.demo.base.Util;

public class PreLoadBean extends BaseBean implements Serializable {

	private static final long serialVersionUID = 1L;

	public int getPersonCount() {
		debug("Preload persons...");
		// Run as signer to allow reading the protected data
		elevateAccess(DataBean.getCurrentInstance().getPersonFacade());
		int n = DataBean.getCurrentInstance().getPersons().size();
		resetAccess(DataBean.getCurrentInstance().getPersonFacade());
		return n;
	}

	public int getCarCount() {
		debug("Preload cars...");
		// Run as signer to allow reading the protected data
		elevateAccess(DataBean.getCurrentInstance().getCarFacade());
		int n = DataBean.getCurrentInstance().getCarFacade().getAllCars().size();
		resetAccess(DataBean.getCurrentInstance().getCarFacade());
		return n;
	}

	private void resetAccess(BaseCRUDFacade facade) {
		if (!Util.isLoggedIn()) {
			facade.setRunAsSigner(false);
		}
	}

	private void elevateAccess(BaseCRUDFacade facade) {
		if (!Util.isLoggedIn()) {
			debug("Elevate access...");
			facade.setRunAsSigner(true);
		}
	}
}
