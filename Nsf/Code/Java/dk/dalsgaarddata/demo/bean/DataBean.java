package dk.dalsgaarddata.demo.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import dk.dalsgaarddata.demo.base.BaseBean;
import dk.dalsgaarddata.demo.base.Util;
import dk.dalsgaarddata.demo.dao.facade.CarCRUDFacade;
import dk.dalsgaarddata.demo.dao.facade.PersonCRUDFacade;
import dk.dalsgaarddata.demo.data.Person;
import dk.dalsgaarddata.demo.view.PersonView;

/**
 * @author jda, 2013.11.13
 * 
 */
public class DataBean extends BaseBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private CarCRUDFacade carFacade = null;
	private PersonCRUDFacade personFacade = null;

	public static DataBean getCurrentInstance() {
		// This is a neat way to get a handle on the instance of this bean in the session scope from other Java code...
		FacesContext context = Util.getFacesContext();
		return (DataBean) context.getApplication().getVariableResolver().resolveVariable(context, "Data");
	}

	public CarCRUDFacade getCarFacade() {
		if (null == carFacade) {
			debug("Create carFacade...");
			carFacade = new CarCRUDFacade();
		}
		/*		
				return CacheContainer.INSTANCE.getCarFacade();
				CarCRUDFacade carFacade = (CarCRUDFacade) ServerBean.getCurrentInstance().get(CarCRUDFacade.class.getName());
				if (null == carFacade) {
					carFacade = new CarCRUDFacade();
					ServerBean.getCurrentInstance().put(CarCRUDFacade.class.getName(), carFacade);
				}
		*/
		return carFacade;
	}

	//	public boolean isPersonFacadeInitialized() {
	//		return CacheContainer.INSTANCE.isPersonFacadeInitialized();
	//	}

	public PersonCRUDFacade getPersonFacade() {
		if (null == personFacade) {
			debug("Create personFacade...");
			personFacade = new PersonCRUDFacade();
		}
		//		return CacheContainer.INSTANCE.getPersonFacade();
		/*
				String TEST_ID = "myTestString";
				// DEBUG - check if object survives....
				String s = (String) ServerBean.getCurrentInstance().get(TEST_ID);
				if (null == s) {
					s = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
					ServerBean.getCurrentInstance().put(TEST_ID, s);
					debug("getPersonFacade: s was NOT set. Set it=" + s);
				} else {
					debug("getPersonFacade: s=" + s);
				}
				//		debug("Classloader=" + ServerBean.getCurrentInstance().getClass().getClassLoader().getClass().getName());
				//		debug("Classloader=" + ServerBean.getCurrentInstance().getClass().getClassLoader().toString());
				try {
					debug("Class.forName=" + Class.forName("dk.dalsgaarddata.demo.dao.facade.PersonCRUDFacade").getClassLoader().toString());
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}

				Object facade = ServerBean.getCurrentInstance().get(PersonCRUDFacade.class.getName());
				if (null == facade) {
					debug("personFacade is null");
				} else {
					debug("personFacade is of class: " + facade.getClass().getName());
				}

				PersonCRUDFacade personFacade = (PersonCRUDFacade) ServerBean.getCurrentInstance().get(PersonCRUDFacade.class.getName());
				if (null == personFacade) {
					personFacade = new PersonCRUDFacade();
					ServerBean.getCurrentInstance().put(PersonCRUDFacade.class.getName(), personFacade);
					debug("Set a new personFacade...");
				} else {
					debug("Use existing personFacade...");
				}
		*/
		debug("Get personFacade...");
		/*
		PersonCRUDFacade personFacade = (PersonCRUDFacade) ServerBean.getCurrentInstance().get(PersonCRUDFacade.class.getName());
		if (null == personFacade) {
			debug("Create personFacade...");
			ServerBean.getCurrentInstance().cacheObject(PersonCRUDFacade.class.getName(), Util.getSession().getCurrentDatabase().getFilePath(), PersonCRUDFacade.class.getName());
			//			personFacade = new PersonCRUDFacade();
			//			ServerBean.getCurrentInstance().put(PersonCRUDFacade.class.getName(), personFacade);
			personFacade = (PersonCRUDFacade) ServerBean.getCurrentInstance().get(PersonCRUDFacade.class.getName());
			debug("Set a new personFacade...");
		} else {
			debug("Use existing personFacade...");
		}
		*/
		return personFacade;
	}

	public List<PersonView> getPersons() {
		List<PersonView> persons = new ArrayList<PersonView>();
		debug("getPersons...");
		for (Person person : getPersonFacade().getAllPersons()) {
			PersonView view = new PersonView(person);
			view.setCars(getPersonFacade().getPersonsCars(person.getKey()));
			persons.add(view);
		}

		return persons;
	}
}
