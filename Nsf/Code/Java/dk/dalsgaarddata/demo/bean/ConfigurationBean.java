package dk.dalsgaarddata.demo.bean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.context.FacesContext;

import org.openntf.domino.xsp.helpers.ServerBean;

import dk.dalsgaarddata.demo.base.BaseBean;
import dk.dalsgaarddata.demo.base.Util;

/**
 * @author jda, 2013.11.13
 * 
 */
public class ConfigurationBean extends BaseBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static boolean debug = true;
	public static String SYSTEM_DATE_FORMAT = "dd-MM-yyyy";
	public static String SYSTEM_DATETIME_FORMAT = "dd-MM-yyyy hh:mm";

	//private final MetaDataBean meta;

	public ConfigurationBean() {
		//meta = new MetaDataBean();
	}

	public static ConfigurationBean getCurrentInstance() {
		// This is a neat way to get a handle on the instance of this bean in the application scope from other Java code...
		FacesContext context = Util.getFacesContext();
		ConfigurationBean bean = (ConfigurationBean) context.getApplication().getVariableResolver().resolveVariable(context, "Configuration");
		return bean;
	}

	public static boolean isDebug() {
		return debug;
	}

	public boolean isDebugEnabled() {
		return debug;
	}

	public MetaDataBean getMeta() {
		//return meta;
		//		return CacheContainer.INSTANCE.getMeta();

		String TEST_ID = "myTestStringMeta";
		// DEBUG - check if object survives....
		String s = (String) ServerBean.getCurrentInstance().get(TEST_ID);
		if (null == s) {
			s = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
			ServerBean.getCurrentInstance().put(TEST_ID, s);
			debug("getMeta: s was NOT set. Set it=" + s);
		} else {
			debug("getMeta: s=" + s);
		}
		MetaDataBean meta = (MetaDataBean) ServerBean.getCurrentInstance().get(MetaDataBean.class.getName());
		if (null == meta) {
			meta = new MetaDataBean();
			ServerBean.getCurrentInstance().put(MetaDataBean.class.getName(), meta);
		}
		return meta;
	}

	public String getSiteRootUrl() {
		return Util.getSiteRootUrl();
	}

}
