package dk.dalsgaarddata.demo.bean;

import java.io.Serializable;

import dk.dalsgaarddata.demo.base.BaseBean;
import dk.dalsgaarddata.demo.base.Util;

/*
 * Well, it is just so much easier to use Java for these silly url manipulations and ask if logged in etc. 
 * - much cleaner than using SSJS.
 */
public class AppSessionBean extends BaseBean implements Serializable {
	private static final long serialVersionUID = 1L;

	public boolean isLoggedIn() {
		return Util.isLoggedIn();
	}

	public String getLoginUrl() {
		return getLogInOutUrl("login");
	}

	public String getLogoutUrl() {
		return getLogInOutUrl("logout");
	}

	private String getLogInOutUrl(String action) {
		return Util.getBaseUrl() + "?" + action + "&redirectto=" + Util.getBaseUrl().replace(Util.getSiteRootUrl(), "");
	}
}
