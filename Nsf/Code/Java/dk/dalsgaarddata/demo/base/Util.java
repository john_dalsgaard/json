package dk.dalsgaarddata.demo.base;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.openntf.domino.Name;
import org.openntf.domino.Session;
import org.openntf.domino.utils.Factory;
import org.openntf.domino.utils.Factory.SessionType;

import com.ibm.commons.util.StringUtil;
import com.ibm.xsp.designer.context.XSPContext;

import dk.dalsgaarddata.demo.bean.ConfigurationBean;

/**
 * @author jda, 2013.11.13
 * 
 */
public class Util {
	public static final String MY_PAGE = "myPage.xsp";
	public static final String LOGIN_PAGE = "index.xsp";

	// This utility class should never be instantiated...
	// Suppress default constructor for utility class
	private Util() {
		// Avoid instantion via reflection
		throw new AssertionError();
	}

	public static Session getSession() {
		System.out.println("getSession...");
		//return Factory.fromLotus(NotesContext.getCurrent().getCurrentSession(), Session.class, null);
		return Factory.getSession();
	}

	public static List<String> getUserRoles() {
		return getUserRoles(getLogonName());
	}

	public static List<String> getUserRoles(String user) {
		return getSession().getCurrentDatabase().queryAccessRoles(user);
	}

	public static Session getSessionAsSigner() {
		System.out.println("getSessionAsSigner...");
		//		return Factory.fromLotus(NotesContext.getCurrent().getSessionAsSigner(), Session.class, null);
		return Factory.getSession(SessionType.SIGNER);
	}

	public static FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}

	public static ExternalContext getExternalContext() {
		return getFacesContext().getExternalContext();
	}

	public static XSPContext getXspContext() {
		return XSPContext.getXSPContext(getFacesContext());
	}

	public static String getLogonName() {
		return getXspContext().getUser().getName();
	}

	public static boolean isLoggedIn() {
		return !"Anonymous".equalsIgnoreCase(getLogonName());
	}

	public static String getAbbreviatedName() {
		return getAbbreviatedName(getLogonName());
	}

	public static String getAbbreviatedName(String name) {
		if (StringUtil.startsWithIgnoreCase(name, "cn=")) {
			Name abbName = getSession().createName(name);
			return abbName.getAbbreviated();
		}
		return name;
	}

	public static String getCurrentUrl() {
		return Util.getXspContext().getUrl().toString();
	}

	public static String getLocalPart(String url) {
		int lastSlash = url.lastIndexOf('/') + 1;
		return url.substring(lastSlash);
	}

	public static String getXPage(String url) {
		String page = getLocalPart(url);
		int xspStart = page.toLowerCase().indexOf(".xsp");
		if (xspStart > 0) {
			page = page.substring(0, xspStart + 4);
		}
		return page;
	}

	public static String getCurrentPage() {
		return getXPage(getCurrentUrl());
	}

	public static HttpServletRequest getRequest() {
		return (HttpServletRequest) getExternalContext().getRequest();
	}

	public static String getReferingUrl() {
		return (getRequest()).getHeader("referer");
	}

	public static String getReferingPage() {
		String page = "/";
		String refUrl = getReferingUrl();
		String refBase = getBaseUrl(refUrl);
		if (refBase.equalsIgnoreCase(getBaseUrl())) {
			// Same site...
			page = getXPage(refUrl);
		}
		return page;
	}

	public static String getBaseUrl() {
		return getBaseUrl(getCurrentUrl());
	}

	public static String getBaseUrl(String url) {
		int lastSlash = url.lastIndexOf('/') + 1;
		return url.substring(0, lastSlash); // includes '/'
	}

	public static String getSiteRootUrl() {
		String siteRoot = getCurrentUrl();
		siteRoot = siteRoot.substring(0, siteRoot.indexOf("/", siteRoot.indexOf("//") + 2));
		return siteRoot;
	}

	// @SuppressWarnings("unchecked")
	// public Map<String, String> getParams() {
	// return getExternalContext().getRequestParameterMap();
	// }

	public static String getUrlParam(String name) {
		return ((HttpServletRequest) getExternalContext().getRequest()).getParameter(name);
	}

	public static void redirectToXpage(String xPage) {
		try {
			String url = getBaseUrl();
			if (null != xPage) {
				if (xPage.toLowerCase().indexOf("http") == 0) {
					// Starts with "http" --> use entire url
					url = xPage;
				} else {
					url = getBaseUrl() + xPage;
				}
			}
			System.out.println("redirect to: " + url);
			getExternalContext().redirect(url);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Finds an UIComponent by its component identifier in the current component tree.
	 * 
	 * @param compId
	 *            the component identifier to search for
	 * @return found UIComponent or null
	 * 
	 * @throws NullPointerException
	 *             if <code>compId</code> is null
	 */
	public static UIComponent findComponent(String compId) {
		return findComponent(FacesContext.getCurrentInstance().getViewRoot(), compId);
	}

	/**
	 * Origin: http://www.mindoo.com/web/blog.nsf/dx/18.07.2009191738KLENAL.htm Finds an UIComponent by its component identifier in the component tree below the specified <code>topComponent</code> top
	 * component.
	 * 
	 * @param topComponent
	 *            first component to be checked
	 * @param compId
	 *            the component identifier to search for
	 * @return found UIComponent or null
	 * 
	 * @throws NullPointerException
	 *             if <code>compId</code> is null
	 */
	@SuppressWarnings("unchecked")
	public static UIComponent findComponent(UIComponent topComponent, String compId) {
		if (compId == null) throw new NullPointerException("Component identifier cannot be null");

		if (compId.equals(topComponent.getId())) return topComponent;

		if (topComponent.getChildCount() > 0) {
			List<UIComponent> childComponents = topComponent.getChildren();

			for (UIComponent currChildComponent : childComponents) {
				UIComponent foundComponent = findComponent(currChildComponent, compId);
				if (foundComponent != null) return foundComponent;
			}
		}
		return null;
	}

	// Validation rules
	public static boolean isEmptyValue(Object value) {
		if (null == value) {
			return true;
		}
		if (value instanceof String) {
			return StringUtil.isEmpty((String) value);
		}
		return null == getNumber(value);
	}

	public static boolean isEmail(String mail) {
		if (StringUtil.isEmpty(mail)) {
			return false;
		}
		return mail.matches("(?!^[.+&_-]*@.*$)(^[_\\w\\d+&-]+(\\.[_\\w\\d+&-]*)*@[\\w\\d-]+(\\.[\\w\\d-]+)*\\.(([\\d]{1,3})|([\\w]{2,}))$)");
	}

	public static Number getNumber(Object value) {
		Number n = null;
		if (value instanceof Number) {
			n = (Number) value;
		} else if (value instanceof String) {
			try {
				n = Double.parseDouble((String) value);
			} catch (NumberFormatException ne) {
				// Do nothing
			}
		}
		return n;
	}

	public static boolean isCheckedValue(Object value) {
		if (value instanceof Boolean) {
			return (Boolean) value;
		}
		if (value instanceof String) {
			return ("true".equalsIgnoreCase((String) value));
		}
		return false;
	}

	public static boolean isCheckBoxChecked(UIComponent component) {
		Object checkBox = Util.getSubmittedValue(component);
		return isCheckedValue(checkBox);
	}

	public static String getDateAsText(Date date) {
		String dt = "";
		if (null != date) {
			dt = (new SimpleDateFormat(ConfigurationBean.SYSTEM_DATE_FORMAT)).format(date);
		}
		return dt;
	}

	public static String getDateTimeAsText(Date date) {
		String dt = "";
		if (null != date) {
			dt = (new SimpleDateFormat(ConfigurationBean.SYSTEM_DATETIME_FORMAT)).format(date);
		}
		return dt;
	}

	public static Object getSubmittedValue(UIComponent c) {
		// value submitted from the browser
		Object o = null;
		if (null != c) {
			o = ((UIInput) c).getSubmittedValue();
			if (null == o) {
				// else not yet submitted
				o = ((UIInput) c).getValue();
			}
		}
		return o;
	}

}
