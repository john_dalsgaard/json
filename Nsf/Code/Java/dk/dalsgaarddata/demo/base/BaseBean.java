package dk.dalsgaarddata.demo.base;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 * @author jda, 2013.11.13
 * 
 */
public class BaseBean extends Base {
	protected String failMessage = null;

	public BaseBean() {
		debug("Instantiating " + this.getClass().getSimpleName());
	}

	public static String buildTypeAheadMarkup(List<SelectItem> choices) {
		StringBuilder returnList = new StringBuilder();
		if (null != choices) {
			returnList.append("<ul>");
			for (SelectItem item : choices) {
				StringBuilder details = new StringBuilder("<li>");
				// details.append(item.getLabel());
				details.append("<span class=\"informal\">" + item.getLabel() + " (" + item.getValue() + ")</span><span style=\"display:none\">" + item.getLabel() + "</span>");
				details.append("</li>");
				returnList.append(details);
			}
			returnList.append("</ul>");
		}
		return returnList.toString();
	}

	private void displayErrorMessage(FacesContext context, UIComponent toValidate, String msg) {
		((UIInput) toValidate).setValid(false);
		debug("Display error message: " + msg + " in field " + toValidate.getId());
		FacesMessage fieldError = new FacesMessage();
		// yourFailure.setDetail("Really you need to promise to never do that again!");
		fieldError.setSummary(msg);
		fieldError.setSeverity(FacesMessage.SEVERITY_FATAL);

		context.addMessage(toValidate.getClientId(context), fieldError);
	}

	protected void displayErrorMessages(Map<String, String> errors) {
		if (null != errors) {
			for (Entry<String, String> err : errors.entrySet()) {
				UIComponent component = Util.findComponent("input" + err.getKey());
				if (null == component) {
					debug("Could not find UI component: input" + err.getKey());
				} else {
					displayErrorMessage(Util.getFacesContext(), component, err.getValue());
				}
			}
		}
	}

	public void setFailMessage(String failMessage) {
		this.failMessage = failMessage;
	}

	public String getFailMessage() {
		return failMessage;
	}

}
