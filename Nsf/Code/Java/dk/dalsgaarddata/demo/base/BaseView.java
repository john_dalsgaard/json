package dk.dalsgaarddata.demo.base;

import java.io.Serializable;

import dk.dalsgaarddata.demo.bean.ConfigurationBean;
import dk.dalsgaarddata.demo.bean.DataBean;
import dk.dalsgaarddata.demo.bean.MetaDataBean;

/**
 * @author jda, 2013.11.13
 * 
 */
public class BaseView extends Base implements Serializable {
	private static final long serialVersionUID = 1L;
	private MetaDataBean meta = null;
	private DataBean data = null;

	protected MetaDataBean getMeta() {
		if (null == meta) {
			meta = ConfigurationBean.getCurrentInstance().getMeta();
		}
		return meta;
	}

	protected DataBean getData() {
		if (null == data) {
			data = DataBean.getCurrentInstance();
		}
		return data;
	}

}