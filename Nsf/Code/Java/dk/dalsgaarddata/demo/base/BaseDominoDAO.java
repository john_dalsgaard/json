/**
 * Base functions for all DominoDAO classes
 */
package dk.dalsgaarddata.demo.base;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.openntf.domino.Database;
import org.openntf.domino.DateTime;
import org.openntf.domino.Document;
import org.openntf.domino.Session;
import org.openntf.domino.View;

import com.ibm.commons.util.StringUtil;

/**
 * @author jda, 2013.11.13
 * 
 */
public class BaseDominoDAO extends Base {

	private Database db = null;
	private boolean viewDirty = false;
	protected boolean runAsSigner = false;

	protected Session getSession() {
		if (runAsSigner) {
			debug("getSession as signer...");
			return Util.getSessionAsSigner();
		}
		//		debug("getSession...");
		return Util.getSession();
	}

	protected boolean isUnid(String key) {
		return (null != key && key.matches("[0-9A-Fa-f]{32}"));
	}

	protected Database getDb() {
		if (null == db) {
			db = getSession().getCurrentDatabase();
		}
		return db;
	}

	protected View getView(String name) {
		return getDb().getView(name);
	}

	protected void setViewDirty(boolean b) {
		viewDirty = b;
	}

	protected boolean isViewDirty() {
		return viewDirty;
	}

	public void setRunAsSigner(boolean b) {
		runAsSigner = b;
	}

	protected boolean updateField(Document doc, String field, String value) {
		if (doc.hasItem(field)) {
			if (StringUtil.isEmpty(value)) { // "" or null
				doc.removeItem(field);
				return true;
			} else {
				// if (value.equals(doc.getItemValueString(field))) {
				if (value.equals(doc.get(field))) {
					return false;
				}
			}
		} else {
			if (StringUtil.isEmpty(value)) { // "" or null
				return false;
			}
		}
		// doc.replaceItemValue(field, value);
		doc.put(field, value);
		return true;
	}

	protected boolean updateField(Document doc, final String field, final List<?> value) {
		if (doc.hasItem(field)) {
			if (isEmptyList(value)) {
				doc.removeItem(field);
				return true;
			} else {
				value.removeAll(Arrays.asList(null, ""));
				if (value.equals(doc.getItemValue(field))) {
					return false;
				}
			}
		} else {
			if (isEmptyList(value)) {
				return false;
			}
			value.removeAll(Arrays.asList(null, ""));
		}
		// doc.replaceItemValue(field, value);
		//		debug("updateField " + field + ": " + value);
		doc.put(field, value);
		return true;
	}

	private boolean isEmptyList(List<?> value) {
		if (null == value) return true;
		if (value.size() == 0) return true;
		for (Object object : value) {
			if (null != object) {
				if (object instanceof String && StringUtil.isNotEmpty((String) object)) {
					return false;
				} else {
					// Assume other object is not "empty"
					return false;
				}
			}
		}
		return true;
	}

	protected boolean updateField(Document doc, String field, boolean value) {
		return updateField(doc, field, fromBoolean(value));
	}

	protected boolean updateField(Document doc, String field, Double value) {
		if (doc.hasItem(field)) {
			if (null == value) {
				doc.removeItem(field);
				return true;
			} else {
				if (value.equals(doc.getItemValueDouble(field))) {
					return false;
				}
			}
		} else {
			if (null == value) {
				return false;
			}
		}
		// doc.replaceItemValue(field, value);
		doc.put(field, value);
		return true;
	}

	protected boolean updateField(Document doc, String field, Integer value) {
		if (doc.hasItem(field)) {
			if (null == value) {
				doc.removeItem(field);
				return true;
			} else {
				if (value.equals(doc.getItemValueInteger(field))) {
					return false;
				}
			}
		} else {
			if (null == value) {
				return false;
			}
		}
		// doc.replaceItemValue(field, value);
		doc.put(field, value);
		return true;
	}

	protected boolean updateField(Document doc, String field, Long value) {
		if (doc.hasItem(field)) {
			if (null == value) {
				doc.removeItem(field);
				return true;
			} else {
				if (value.equals(doc.get(field))) {
					return false;
				}
			}
		} else {
			if (null == value) {
				return false;
			}
		}
		// doc.replaceItemValue(field, value);
		doc.put(field, value);
		return true;
	}

	protected boolean updateField(Document doc, String field, Date value) {
		if (doc.hasItem(field)) {
			if (null == value) {
				doc.removeItem(field);
				return true;
			} else {
				Date dt = toDate(doc.get(field));
				if (value.equals(dt)) {
					return false;
				}
			}
		} else {
			if (null == value) {
				return false;
			}
		}
		// doc.replaceItemValue(field, value);
		doc.put(field, value);
		return true;
	}

	protected String fromBoolean(boolean b) {
		return b ? "1" : "";
	}

	protected boolean toBoolean(String s) {
		return "1".equals(s);
	}

	protected Integer toInteger(Object obj) {
		Double d = toDouble(obj);
		if (null == d) {
			return null;
		} else {
			return d.intValue();
		}
	}

	protected Double toDouble(Object obj) {
		//		debug("toDouble(" + obj + ") - obj.class=" + obj.getClass().getSimpleName());
		if (null != obj) {
			if (obj instanceof Double) {
				return (Double) obj;
			}
			try {
				if (obj instanceof String) {
					String s = (String) obj;
					return Double.valueOf(s);
				}
			} catch (NumberFormatException nfe) {
				// Ignore.... Return zero
			}
		}
		return null;
	}

	protected Date toDate(Object obj) {
		//		debug("toDate(" + obj + ") - obj.class=" + obj.getClass().getSimpleName());
		if (null != obj) {
			if (obj instanceof Date) {
				return (Date) obj;
			}
			if (obj instanceof DateTime) {
				DateTime dt = (DateTime) obj;
				return dt.toJavaDate();
			}
			if (obj instanceof String) {
				String s = (String) obj;
				if (StringUtil.isEmpty(s)) {
					return null;
				}
				// Date d = new Date(s);
				Date d = null;
				try {
					d = new SimpleDateFormat("dd/mm/yyyy").parse(s);
				} catch (ParseException e) {
					// Try another format
					try {
						d = new SimpleDateFormat("dd-mm-yyyy").parse(s);
					} catch (ParseException e1) {
						// Ignore - just return null
					}
				}

				return d;
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	protected List<String> toList(Object obj) {
		//		debug("toList(" + obj + ") - obj.class=" + obj.getClass().getSimpleName());
		List<String> list = new ArrayList<String>();
		if (null != obj) {
			if (obj instanceof Vector) {
				Vector v = (Vector) obj;
				for (int i = 0; i < v.size(); i++) {
					String e = (String) v.elementAt(i);
					if (StringUtil.isNotEmpty(e)) {
						list.add(e);
					}
				}
				// return new ArrayList<String>((Vector) obj);
			} else if (obj instanceof String) {
				String s = (String) obj;
				if (StringUtil.isNotEmpty(s)) {
					list.add(s);
				}
			}
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	protected List<Date> toDateList(Object obj) {
		//		debug("toDateList(" + obj + ") - obj.class=" + obj.getClass().getSimpleName());
		List<Date> list = new ArrayList<Date>();
		if (null != obj) {
			if (obj instanceof Vector) {
				Vector v = (Vector) obj;
				for (int i = 0; i < v.size(); i++) {
					Date dt = toDate(v.elementAt(i));
					if (null != dt) {
						list.add(dt);
					}
				}
			} else {
				Date dt = toDate(obj);
				if (null != dt) {
					list.add(dt);
				}
			}
		}
		return list;
	}
}
