package dk.dalsgaarddata.demo.data;

import dk.dalsgaarddata.demo.base.BaseData;

/**
 * @author jda, 2013.11.13
 * 
 */
public class Car extends BaseData {
	public static enum AcceptedBrands {
		MERCEDES("Mercedes"), AUDI("Audi"), PORSCHE("Porsche"), BMW("BMW"), FERRARI("Ferrari"), MUSTANG("Ford Mustang"), CORVETTE("Corvette");
		private final String value_;

		private AcceptedBrands(final String value) {
			value_ = value;
		}

		public String getValue() {
			return value_;
		}
	}

	private static final long serialVersionUID = 1L;
	private String personKey = null;
	private String brand = null;
	private String model = null;
	private String colour = null;
	private boolean convertible = false;
	private Integer doors = null;

	public String getPersonKey() {
		return personKey;
	}

	public String getBrand() {
		return brand;
	}

	public String getModel() {
		return model;
	}

	public String getColour() {
		return colour;
	}

	public boolean isConvertible() {
		return convertible;
	}

	public Integer getDoors() {
		return doors;
	}

	public void setPersonKey(String personKey) {
		this.personKey = personKey;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public void setDoors(Integer doors) {
		this.doors = doors;
	}

	public void setConvertible(boolean convertible) {
		this.convertible = convertible;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((brand == null) ? 0 : brand.hashCode());
		result = prime * result + ((colour == null) ? 0 : colour.hashCode());
		result = prime * result + (convertible ? 1231 : 1237);
		result = prime * result + ((doors == null) ? 0 : doors.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((personKey == null) ? 0 : personKey.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!super.equals(obj)) return false;
		if (getClass() != obj.getClass()) return false;
		Car other = (Car) obj;
		if (brand == null) {
			if (other.brand != null) return false;
		} else if (!brand.equals(other.brand)) return false;
		if (colour == null) {
			if (other.colour != null) return false;
		} else if (!colour.equals(other.colour)) return false;
		if (convertible != other.convertible) return false;
		if (doors == null) {
			if (other.doors != null) return false;
		} else if (!doors.equals(other.doors)) return false;
		if (model == null) {
			if (other.model != null) return false;
		} else if (!model.equals(other.model)) return false;
		if (personKey == null) {
			if (other.personKey != null) return false;
		} else if (!personKey.equals(other.personKey)) return false;
		return true;
	}

}
