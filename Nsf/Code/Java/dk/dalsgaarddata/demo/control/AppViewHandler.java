package dk.dalsgaarddata.demo.control;

import java.io.IOException;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.ibm.xsp.application.ViewHandlerExImpl;
import com.ibm.xsp.designer.context.XSPContext;

import dk.dalsgaarddata.demo.base.Util;

public class AppViewHandler extends ViewHandlerExImpl {

	private void debug(String m) {
		System.out.println("AppViewHandler (demo): " + m);
	}

	public AppViewHandler(ViewHandler viewHandler) {
		super(viewHandler);
	}

	@Override
	public UIViewRoot createView(final FacesContext context, String pageName) {
		// Page name is in the format "/home"
		String currentPage = pageName.substring(1); // Remove "/" from "/page" to return "page"
		System.out.println("AppViewHandler: pageName=" + pageName);
		/*		if ("/frontPage".equalsIgnoreCase(pageName)) {
					System.out.println("AppViewHandler: redirect to /index");
					return super.createView(context, "/index");
				}
		*/debug("currentPage=" + currentPage);
		ExternalContext ext = context.getExternalContext();
		String currentUser = ext.getUserPrincipal().getName();
		debug("currentUser=" + currentUser);
		String url = XSPContext.getXSPContext(FacesContext.getCurrentInstance()).getUrl().toString();
		debug("url=" + url);
		int lastSlash = url.lastIndexOf('/') + 1;
		String currentUrl = url.substring(0, lastSlash); // includes '/'
		debug("currentUrl=" + currentUrl);
		//		debug("localPart=" + Util.getLocalPart(url));
		if ("Anonymous".equalsIgnoreCase(currentUser)) {
			if (!"index".equalsIgnoreCase(currentPage) && !"preload".equalsIgnoreCase(currentPage)) {
				debug("User is not logged in. Redirect to index...");
				try {
					ext.redirect(currentUrl + Util.LOGIN_PAGE);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			if ("index".equalsIgnoreCase(currentPage)) {
				try {
					debug("User: " + currentUser + ". Redirect to myPage...");
					ext.redirect(Util.getBaseUrl() + Util.MY_PAGE);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return super.createView(context, pageName);
	}

}
