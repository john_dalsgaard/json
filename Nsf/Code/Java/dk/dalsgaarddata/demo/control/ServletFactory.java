package dk.dalsgaarddata.demo.control;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ibm.domino.services.ServiceEngine;
import com.ibm.domino.services.rest.das.view.RestViewJsonService;
import com.ibm.domino.services.rest.das.view.impl.DefaultViewParameters;
import com.ibm.xsp.extlib.services.servlet.DefaultServiceFactory;
import com.ibm.xsp.extlib.services.servlet.DefaultServletFactory;
import com.ibm.xsp.extlib.services.servlet.ServiceFactory;

/* 
 * Must be registered in /Code/Java/META-INF/services/com.ibm.xsp.adapter.servletFactory as well.... 
 * url to call: .../json.nsf/xsp/services/Persons
 */

public class ServletFactory extends DefaultServletFactory {
	private static ServiceFactory createFactory() {
		DefaultServiceFactory factory = new DefaultServiceFactory();

		factory.addFactory("Persons", new ServiceFactory() {
			public ServiceEngine createEngine(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws ServletException {
				//				System.out.println("createFactory: Persons...");
				DefaultViewParameters p = new DefaultViewParameters();
				p.setViewName("Persons");
				p.setGlobalValues(DefaultViewParameters.GLOBAL_ALL);
				p.setSystemColumns(DefaultViewParameters.SYSCOL_FORM);
				p.setDefaultColumns(true);
				//Set the default parameters
				p.setStart(0);
				p.setCount(4);
				//				System.out.println("createFactory: Persons - return RestViewJsonService...");
				return new RestViewJsonService(httpRequest, httpResponse, p);
			}
		});
		return factory;
	}

	public ServletFactory() {
		super("services", "Extension Library Services Servlet", createFactory());
		//		System.out.println("Creating ServletFactory...");
	}
}