package dk.dalsgaarddata.demo.service;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.servlet.http.HttpServletResponse;

import dk.dalsgaarddata.demo.base.Util;
import dk.dalsgaarddata.demo.view.PersonView;
import dk.dalsgaarddata.ext.gson.GsonWrapper;

public class Controller {
	public static final String ISO_DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZZ";

	private void debug(String m) {
		System.out.println(this.getClass().getSimpleName() + ": " + m);
	}

	protected void error(String m) {
		System.err.println(this.getClass().getSimpleName() + " ERROR - " + m);
	}

	public class Result {
		// Declare fields to be used to generate json
		@SuppressWarnings("unused")
		private boolean success = false;
		@SuppressWarnings("unused")
		private String dataType = null;
		@SuppressWarnings("unused")
		private Error error = null;
		@SuppressWarnings("unused")
		private Object data = null;

		public Result(Object obj, boolean isError) {
			debug("new error - " + isError);
			if (isError) {
				setData(null, (Error) obj, false);
			} else {
				setData(obj, null, true);
			}
		}

		public Result(Object obj) {
			if (null == obj) {
				setData(null, null, true);
			} else {
				if (obj instanceof Error) {
					setData(null, (Error) obj, false);
				} else {
					setData(obj, null, true);
				}
			}
		}

		private void setData(Object data, Error error, boolean success) {
			this.data = data;
			if (null != data) {
				if (data instanceof Collection<?>) {
					for (Object obj : (Collection<?>) data) {
						if (null != obj) {
							this.dataType = obj.getClass().getSimpleName();
							break;
						}
					}
				} else {
					this.dataType = data.getClass().getSimpleName();
				}
			}
			this.error = error;
			this.success = success;
		}

	}

	private void sendResponse(Object data) {
		HttpServletResponse response = (HttpServletResponse) Util.getExternalContext().getResponse();
		ResponseWriter out = Util.getFacesContext().getResponseWriter();
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "no-cache");
		response.setCharacterEncoding("utf-8");
		try {
			// debug("Return JSON result.");
			Result result = new Result(data);
			// Requires Gson JAR to be put in the jvm ext library... - and AllPermissions set in java.pol/java.policy !!!!
			// ... alernatively wrapping it in an OSGi plugin (with AccessController.doPrivileged(....))
			// --> GsonWrapper json = new GsonWrapper(ISO_DATETIME_FORMAT);
			// Gson json = new GsonBuilder().setDateFormat(ISO_DATETIME_FORMAT).create();
			GsonWrapper json = new GsonWrapper(ISO_DATETIME_FORMAT);
			out.write(json.toJson(result));
			//			debug("Output written...");
		} catch (IOException e) {
			error("Could not write JSON to response: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			error("Could not generate JSON for response: " + e.getMessage());
			e.printStackTrace();
		}
		response.setStatus(HttpServletResponse.SC_OK); // Can be used to signal errors...
		// Stop the page from further processing;
		FacesContext.getCurrentInstance().responseComplete();
		//		debug("response complete");
	}

	public void process() throws IOException {
		String method = Util.getRequest().getMethod();
		if ("POST".equalsIgnoreCase(method)) {
			//processPostRequest();
		} else {
			processGetRequest();
		}
	}

	private void doGetPersons() {
		List<PersonView> persons = dk.dalsgaarddata.demo.bean.DataBean.getCurrentInstance().getPersons();
		if (null == persons) {
			sendResponse(new Error("Could not get persons!"));
		} else {
			sendResponse(persons);
		}
	}

	private void processGetRequest() {
		String command = Util.getUrlParam("command");
		debug("Process request: " + command);
		if ("getPersons".equalsIgnoreCase(command)) {
			doGetPersons();
		} else if ("getCars".equalsIgnoreCase(command)) {
			// doGetCars(Util.getUrlParam("usr"));
		}
	}
}
