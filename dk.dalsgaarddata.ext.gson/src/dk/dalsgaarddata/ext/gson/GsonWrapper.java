package dk.dalsgaarddata.ext.gson;

import java.lang.reflect.Type;
import java.security.AccessController;
import java.security.PrivilegedExceptionAction;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author jda
 * @version 1.0.5
 * 
 * Date: 2013.12.30
 * 
 * This utility class wraps a call to Gson into a priviledged call to allow elevation without having 
 * to grant "allaccess" to all jars in the jvm/lib/ext
 * 
 * This class needs to be exported to a jar file and placed in the jvm/lib/ext library for it to be able handle
 * the priviledged calls.
 * 
 * Version 1.0.1:	Added support for specific date formats (and a better default format if none specified)
 * 					Added support for specifying a list of field names to exclude 
 * Version 1.0.2:	Moved to dk.dtu.aqua.ext package 
 * Version 1.0.3:	Default date format changed to not include millis (not present on Domino)
 * Version 1.0.4:	Added fromJson method
 * Version 1.0.5:	Added more signatures to fromJson method
 */
public class GsonWrapper {

	private static final String DFT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private Gson gson = null;
	private String result = null;
	private Object userClass = null;
	private String[] excludeFields = null;

	private class FieldExclusion implements ExclusionStrategy {

		public boolean shouldSkipField(FieldAttributes f) {
			if (null != excludeFields) {
				for (String s : excludeFields) {
					if (s != null && f != null && s.equalsIgnoreCase(f.getName())) {
						return true;
					}
				}
			}
			return false;
		}

		@Override
		public boolean shouldSkipClass(Class<?> arg0) {
			return false;
		}

	}

	/* Constructors... */
	public GsonWrapper() {
		this(DFT_DATE_FORMAT);
	}

	public GsonWrapper(final String[] fields) {
		this(DFT_DATE_FORMAT, fields);
	}

	public GsonWrapper(final String dateFormat) {
		this(dateFormat, null);
	}

	public GsonWrapper(final String dateFormat, final String[] fields) {
		excludeFields = fields;
		gson = new GsonBuilder().setExclusionStrategies(new FieldExclusion()).setDateFormat(dateFormat).create();
	}

	public void toJsonImpl(Object obj) {
		result = gson.toJson(obj);
	}

	public void fromJsonImpl(String obj, Class<?> cls) {
		userClass = gson.fromJson(obj, cls);
	}

	public void fromJsonImpl(String obj, Type type) {
		userClass = gson.fromJson(obj, type);
	}

	@SuppressWarnings( { "unchecked" })
	public String toJson(final Object obj) throws Exception {
		AccessController.doPrivileged(new PrivilegedExceptionAction() {
			public Object run() throws Exception {
				GsonWrapper.this.toJsonImpl(obj);
				return null;
			}
		});
		return result;
	}

	@SuppressWarnings( { "unchecked" })
	public Object fromJson(final String obj, final Class cls) throws Exception {
		AccessController.doPrivileged(new PrivilegedExceptionAction() {
			public Object run() throws Exception {
				GsonWrapper.this.fromJsonImpl(obj, cls);
				return null;
			}
		});
		return userClass;
	}

	@SuppressWarnings("unchecked")
	public Object fromJson(final String obj, final Type type) throws Exception {
		AccessController.doPrivileged(new PrivilegedExceptionAction() {
			public Object run() throws Exception {
				GsonWrapper.this.fromJsonImpl(obj, type);
				return null;
			}
		});
		return userClass;
	}

}
